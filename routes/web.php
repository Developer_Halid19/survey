<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/{any}', function () {
  return view('welcome');
})->where('any', '.*');


Route::group(['prefix' => 'api'], function() {
  Route::namespace('User')->group(function() {

    Route::post('/profile', 'ProfileController@edit');

    Route::put('/profile', 'ProfileController@update');
  });
});