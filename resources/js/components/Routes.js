import React from "react";
import { Switch, Route } from "react-router-dom";

// pages
import Profile from "./pages/Profile";
import User from "./pages/User";
import Cart from "./pages/Cart";
import Setup from "./pages/Setup";

function Routes() {
  return (
    <Switch>
      <Route path="/profile" exact component={Profile} />
      <Route path="/users" component={User} />
      <Route path="/carts" component={Cart} />
      <Route path="/setup" component={Setup} />
    </Switch>
  );
}

export default Routes;
