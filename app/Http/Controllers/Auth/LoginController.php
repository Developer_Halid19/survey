<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Http\Requests\UserLoginRequest;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

use App\Models\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api')->only('logout');
    }

    /**
     * login to system
     *
     */
    public function login(UserLoginRequest $request)
    {
      $user = User::whereEmail($request->email)->first();

      if ($user && Hash::check($request->password, $user->password)) {
        $user->api_token = Str::random(60);
        $user->save();

        $roles = $user->roles()->get(['roles.name', 'roles.slug']);

        return response()->json([
          'success' => true,
          'userId' => $user->id,
          'apiToken' => $user->api_token,
          'name' => $user->name,
          'roles' => $roles
        ]);
      }

      return response()->json([
        'errors' => [
          'email' => ["Почта или пароль, который вы указали, не соответствует ни одному аккаунту."]
        ]
      ], 422);
    }

    /**
     * logout from system
     *
     */
    public function logout(Request $request)
    {
      $user = Auth::user();
      $user->api_token = null;
      $user->save();

      return response()->json([
        'success' => true
      ]);
    }
}
