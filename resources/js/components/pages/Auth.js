import React, { useContext, useEffect, useDebugValue } from "react";
import { Formik, Form, Field } from "formik";
import * as yup from "yup";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import { useSnackbar } from "notistack";
import axios from "axios";

import { requestLogin, auth } from "../../stores/auth";

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}));

const validationSchema = yup.object().shape({
  email: yup
    .string()
    .email("Не валидная почта")
    .required("Введите адрес электронной почты"),
  password: yup.string().required("Введите свой пароль")
});

const Auth = () => {
  const classes = useStyles();
  const { enqueueSnackbar: setSnackbar } = useSnackbar();

  // submit
  const handleSubmit = values => {
    requestLogin(values)
      .then(data =>
        data.success
          ? showSuccess("Вы успешно вошли систему")
          : showWarning("Что-то пошло не так, ваше данные не будут сохранены!")
      )
      .catch(({ response }) => showErrors(response.data.errors));
  };

  // show success in the snackbar
  const showSuccess = message => {
    setSnackbar(message, { variant: "success" });
  };

  // show warning in the snackbar
  const showWarning = message => {
    setSnackbar(message, { variant: "warning" });
  };

  // show error in the snackbar
  const showError = message => {
    setSnackbar(message, { variant: "error" });
  };

  // show errors in the snackbar
  const showErrors = errors => {
    Object.keys(errors)
      .reduce((messages, key) => [...errors[key], ...messages], [])
      .forEach(message => showError(message));
  };

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Войти в систему
        </Typography>
        <Formik
          onSubmit={handleSubmit}
          initialValues={{
            email: "",
            password: ""
          }}
          validationSchema={validationSchema}
        >
          {({
            values,
            touched,
            errors,
            handleChange,
            handleBlur,
            isSubmitting
          }) => (
            <Form className={classes.form}>
              <Field
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="email"
                label="E-mail"
                name="email"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.email}
                as={TextField}
                error={touched.email && !!errors.email}
                helperText={touched.email && errors.email}
              />
              {values.email && !errors.email ? (
                <Field
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  name="password"
                  label="Пароль"
                  type="password"
                  id="password"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.password}
                  as={TextField}
                  error={touched.password && !!errors.password}
                  helperText={touched.password && errors.password}
                />
              ) : null}

              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
              >
                Войти
              </Button>
            </Form>
          )}
        </Formik>
      </div>
    </Container>
  );
};

export default Auth;
