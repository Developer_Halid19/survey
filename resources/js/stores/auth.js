import axios from "axios";
import { createStore, createEvent, createEffect, clearNode } from "effector";
import {
  getUserFromLocalStorage,
  saveUserToLocalStorage,
  changeUserToLocalStorage,
  removeUserFromLocalStorage
} from "../helpers/auth";

// requests
const requestLogin = createEffect();
const requestLogout = createEffect();

requestLogin.use(user => {
  return axios
    .post(`${BASE_URL}/api/auth/login`, user)
    .then(response => response.data);
});

requestLogout.use(apiToken => {
  const headers = {
    Authorization: `Bearer ${apiToken}`
  };

  return axios
    .post(`${BASE_URL}/api/auth/logout`, null, { headers })
    .then(response => response.data);
});

// events
const onSaveUserToLocalStorage = createEvent();
const onChangeUserToLocalStorage = createEvent();
const onRemoveUserFromLocalStorage = createEvent();

onSaveUserToLocalStorage.watch(saveUserToLocalStorage);
onChangeUserToLocalStorage.watch(changeUserToLocalStorage);
onRemoveUserFromLocalStorage.watch(removeUserFromLocalStorage);

requestLogin.done.watch(({ result }) => {
  if (result.success) {
    // data about the user, save to localStorage
    onSaveUserToLocalStorage(result);
  }
});

requestLogout.watch(
  // data about the user, remove from localStorage
  onRemoveUserFromLocalStorage
);

// initial state

const user = getUserFromLocalStorage();

function checkAuthenticated(data) {
  return (
    data.userId !== null &&
    data.name !== null &&
    data.apiToken !== null &&
    data.roles.length > 0
  );
}

const auth = createStore({
  ...user,
  isAuthenticated: checkAuthenticated(user)
});

const userRolesName = auth.map(user => {
  return user.roles.map(role => role.name);
});

const userRolesSlug = auth.map(user => {
  return user.roles.map(role => role.slug);
});

// apply events

auth.on(onSaveUserToLocalStorage, (_, { userId, name, roles, apiToken }) => {
  return {
    userId,
    name,
    apiToken,
    roles,
    isAuthenticated: true
  };
});

auth.on(onChangeUserToLocalStorage, (oldState, newState) => {
  return {
    ...oldState,
    ...newState,
    isAuthenticated:
      checkAuthenticated(oldState) || checkAuthenticated(newState)
  };
});

auth.on(onRemoveUserFromLocalStorage, () => ({
  userId: null,
  name: null,
  apiToken: null,
  roles: [],
  isAuthenticated: false
}));

export {
  auth,
  requestLogin,
  requestLogout,
  onSaveUserToLocalStorage,
  onRemoveUserFromLocalStorage,
  onChangeUserToLocalStorage,
  userRolesName,
  userRolesSlug
};
