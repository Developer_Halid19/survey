import React, { useState, useMemo, useEffect } from "react";
import { useStore } from "effector-react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import { makeStyles } from "@material-ui/core/styles";
import { Formik, Form, Field } from "formik";
import * as yup from "yup";
import TextField from "@material-ui/core/TextField";
import IconButton from "@material-ui/core/IconButton";
import PhotoCamera from "@material-ui/icons/PhotoCamera";
import Content from "../layouts/Content";
import CircularProgress from "@material-ui/core/CircularProgress";
import { useSnackbar } from "notistack";
import {
  auth,
  userRolesName,
  onSaveUserToLocalStorage,
  onChangeUserToLocalStorage
} from "../../stores/auth";
import axios from "axios";

const useFormStyles = makeStyles({
  field: {
    width: "100%",
    marginBottom: 30
  },
  upload: {
    display: "none"
  }
});

const validationSchema = yup.object().shape({
  email: yup
    .string()
    .email("Не валидная почта")
    .required("Введите адрес электронной почты"),
  name: yup.string().required("Введите свое имя")
});

const UserForm = ({ user }) => {
  const classes = useFormStyles();
  const { enqueueSnackbar: setSnackbar } = useSnackbar();

  const handleSubmit = values => {
    const { apiToken } = user;
    const headers = {
      Authorization: `Bearer ${apiToken}`
    };

    axios
      .put(`${BASE_URL}/api/profile`, values, { headers })
      .then(response => response.data)
      .then(data => {
        if (data.success) {
          onChangeUserToLocalStorage(data.user);
          showSuccess("Ваше данные были изменены");
        } else {
          showError("Что-то пошло не так, ваши данные не были изменены!");
        }
      });
  };

  // show success in the snackbar
  const showSuccess = message => {
    setSnackbar(message, { variant: "success" });
  };

  // show error in the snackbar
  const showError = message => {
    setSnackbar(message, { variant: "error" });
  };

  return (
    <Formik initialValues={user} onSubmit={handleSubmit}>
      {({ values, handleChange, isSubmitting }) => (
        <Form>
          <Field
            name="email"
            value={values.email}
            label="E-mail"
            onChange={handleChange}
            as={TextField}
            variant="outlined"
            className={classes.field}
          />
          <Field
            name="name"
            value={values.name}
            label="Имя"
            onChange={handleChange}
            as={TextField}
            variant="outlined"
            className={classes.field}
          />
          {/* <Field
            name="password"
            value={values.password}
            label="Пароль"
            onChange={handleChange}
            as={TextField}
            variant="outlined"
            className={classes.field}
          /> */}

          <Button
            type="submit"
            // disabled={isSubmitting}
            variant="outlined"
            color="primary"
          >
            Сохранить
          </Button>

          <input
            accept="image/*"
            className={classes.upload}
            id="icon-button-file"
            type="file"
          />
          <label htmlFor="icon-button-file">
            <IconButton
              color="primary"
              aria-label="upload picture"
              component="span"
            >
              <PhotoCamera />
            </IconButton>
          </label>
        </Form>
      )}
    </Formik>
  );
};

const useProfileStyles = makeStyles({
  user: {
    padding: 20
  },
  userAvatar: {
    height: 100,
    width: 100
  },
  userForm: {
    padding: 20
  },
  progress: {
    textAlign: "center"
  }
});

const Profile = () => {
  const classes = useProfileStyles();
  const { apiToken, name, roles } = useStore(auth);
  const rolesName = useStore(userRolesName);
  const [user, setUser] = useState(null);

  const joinRolesName = useMemo(() => rolesName.join(" / "), []);

  useEffect(() => {
    const headers = {
      Authorization: `Bearer ${apiToken}`
    };

    axios
      .post(`${BASE_URL}/api/profile`, null, { headers })
      .then(response => response.data)
      .then(data => {
        if (data.success) {
          setUser(data.user);
        }
      });
  }, []);

  return (
    <Content title="Профиль">
      <Grid container spacing={3}>
        <Grid item xs={4}>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <Card className={classes.user}>
                <CardHeader
                  avatar={
                    <Avatar
                      aria-label="recipe"
                      className={classes.userAvatar}
                    />
                  }
                  title={name}
                  subheader={joinRolesName}
                />
              </Card>
            </Grid>
            <Grid item xs={12}>
              <Card className={classes.userForm}>
                {user ? (
                  <UserForm user={user} />
                ) : (
                  <div className={classes.progress}>
                    <CircularProgress disableShrink />
                  </div>
                )}
              </Card>
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={8}>
          <Card>
            <CardContent>Нет Информации</CardContent>
          </Card>
        </Grid>
      </Grid>
    </Content>
  );
};

export default Profile;
