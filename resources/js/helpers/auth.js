export function getUserFromLocalStorage() {
  return {
    userId: localStorage.getItem("userId"),
    name: localStorage.getItem("name"),
    roles: JSON.parse(localStorage.getItem("roles") || "[]"),
    apiToken: localStorage.getItem("apiToken")
  };
}

export function saveUserToLocalStorage({ userId, name, roles, apiToken }) {
  localStorage.setItem("userId", userId);
  localStorage.setItem("name", name);
  localStorage.setItem("roles", JSON.stringify(roles));
  localStorage.setItem("apiToken", apiToken);
}

export function changeUserToLocalStorage({ userId, name, roles, apiToken }) {
  userId && localStorage.setItem("userId", userId);
  name && localStorage.setItem("name", name);
  roles &&
    roles.length > 0 &&
    localStorage.setItem("roles", JSON.stringify(roles));
  apiToken && localStorage.setItem("apiToken", apiToken);
}

export function removeUserFromLocalStorage() {
  localStorage.removeItem("userId");
  localStorage.removeItem("name");
  localStorage.removeItem("roles");
  localStorage.removeItem("apiToken");
}
