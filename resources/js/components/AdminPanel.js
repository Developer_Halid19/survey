import React from "react";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";

// layouts
import DrawerNavigation from "./layouts/DrawerNavigation";
import TopPanel from "./layouts/TopPanel";

// routes
import Routes from "./Routes";

const useStyle = makeStyles({
  container: {
    height: "100%"
  },

  navigation: {
    width: "15%"
  },

  content: {
    width: "85%"
  }
});

const AdminPanel = () => {
  const classes = useStyle();

  return (
    <Grid container className={classes.container}>
      <div className={classes.navigation}>
        <DrawerNavigation />
      </div>
      <div className={classes.content}>
        <TopPanel />
        <Routes />
      </div>
    </Grid>
  );
};

export default AdminPanel;
