import React, { useMemo } from "react";
import { useHistory } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import Avatar from "@material-ui/core/Avatar";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Badge from "@material-ui/core/Badge";
import axios from "axios";
import { useSnackbar } from "notistack";
import { useStore } from "effector-react";

// icons
import PermIdentityIcon from "@material-ui/icons/PermIdentity";
import PeopleOutlineIcon from "@material-ui/icons/PeopleOutline";
import DeleteOutlineIcon from "@material-ui/icons/DeleteOutline";
import SettingsIcon from "@material-ui/icons/Settings";
import PersonAddIcon from "@material-ui/icons/PersonAdd";
import LibraryBooksIcon from "@material-ui/icons/LibraryBooks";
import PostAddIcon from "@material-ui/icons/PostAdd";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";

import { auth, requestLogout, userRolesName } from "../../stores/auth";

const useNavListItemStyles = makeStyles({
  listItem: {
    fontSize: 12,
    borderRadius: 5,
    padding: "10px 30px 10px 10px",
    "& a": {
      display: "flex",
      width: "100%",
      color: "#fff",
      textDecoration: "none"
    }
  },
  listItemIcon: {
    marginRight: 10,
    "& svg": {
      color: "rgb(238, 238, 238)",
      opacity: 0.5
    }
  },
  listItemText: {
    "& span": {
      fontSize: 14
    }
  }
});

const NavListItem = ({ nav }) => {
  const classes = useNavListItemStyles();
  const history = useHistory();

  return (
    <List>
      {nav.map(item => {
        return (
          <ListItem
            key={item.title}
            button
            className={classes.listItem}
            onClick={() => history.push(item.to)}
          >
            <span className={classes.listItemIcon}>{item.icon}</span>
            <ListItemText className={classes.listItemText}>
              {item.title}
            </ListItemText>
            {item.size ? (
              <Badge badgeContent={item.size} color="primary"></Badge>
            ) : null}
          </ListItem>
        );
      })}
    </List>
  );
};

const useDrawerNavigationStyles = makeStyles(theme => ({
  drawer: {
    width: "100%",
    flexShrink: 0,
    height: "100%"
  },
  drawerPaper: {
    position: "unset",
    backgroundColor: "rgb(27, 36, 48)",
    color: "rgb(238, 238, 238)"
  },
  toolbar: {
    backgroundColor: "rgb(35, 47, 62)",
    padding: "20px 0",
    ...theme.mixins.toolbar
  },
  toolbarUser: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },
  toolbarAvatar: {
    marginBottom: 10
  },
  toolbarUserName: {
    marginBottom: 5
  },
  line: {
    width: "100%",
    borderBottom: 0
  }
}));

const DrawerNavigation = () => {
  const classes = { ...useDrawerNavigationStyles(), ...useNavListItemStyles() };
  const { enqueueSnackbar: setSnackbar } = useSnackbar();
  const { name, apiToken } = useStore(auth);
  const rolesName = useStore(userRolesName);
  const fullNameRole = useMemo(() => rolesName.join(" / "), [rolesName]);

  const handleLogout = () => {
    requestLogout(apiToken).then(
      data => !data.success && showWarning("Что-то пошло не так")
    );
  };

  // show warning in the snackbar
  const showWarning = message => {
    setSnackbar(message, { variant: "warning" });
  };

  return (
    <Drawer
      className={classes.drawer}
      variant="permanent"
      classes={{
        paper: classes.drawerPaper
      }}
    >
      <div className={classes.toolbar}>
        <div className={classes.toolbarUser}>
          <Avatar className={classes.toolbarAvatar} />
          <div className={classes.toolbarUserName}>{name}</div>
          <div className={classes.toolbarUserRole}>
            <small>{fullNameRole}</small>
          </div>
        </div>
      </div>

      <NavListItem
        nav={[
          {
            title: "Профиль",
            to: "/profile",
            icon: <PermIdentityIcon />,
            size: 0
          },
          {
            title: "Листы",
            to: "/lists",
            icon: <LibraryBooksIcon />,
            size: 0
          },
          {
            title: "Сотрудники",
            to: "/users",
            icon: <PeopleOutlineIcon />,
            size: 0
          },
          {
            title: "Карзина",
            to: "/carts",
            icon: <DeleteOutlineIcon />,
            size: 0
          },
          {
            title: "Настройки",
            to: "/setup",
            icon: <SettingsIcon />,
            size: 0
          }
        ]}
      />

      <hr className={classes.line} />

      <NavListItem
        nav={[
          {
            title: "Добавить сотрудника",
            to: "/user-add",
            icon: <PersonAddIcon />,
            size: 0
          },
          {
            title: "Добавить лист",
            to: "/list-add",
            icon: <PostAddIcon />,
            size: 0
          }
        ]}
      />

      <hr className={classes.line} />

      <List>
        <ListItem button className={classes.listItem} onClick={handleLogout}>
          <span className={classes.listItemIcon}>{<ExitToAppIcon />}</span>
          <ListItemText className={classes.listItemText}>Выйти</ListItemText>
        </ListItem>
      </List>
    </Drawer>
  );
};

export default DrawerNavigation;
