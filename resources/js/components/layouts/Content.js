import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

const useStyle = makeStyles({
  main: {
    padding: '25px 40px'
  },
  title: {
    fontWeight: 400,
    fontSize: 20
  },
  border: {
    borderBottom: '1px solid #aaa',
    borderTop: 0
  },
  content: {
    paddingTop: 20
  }
});

const Content = ({ children, title }) => {
  const classes = useStyle();

  return (
    <div className={classes.main}>
      <h3 className={classes.title}>{title}</h3>
      <hr className={classes.border} />
      <div className={classes.content}>{children}</div>
    </div>
  );
};

export default Content;
