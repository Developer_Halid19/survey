import React, { useState, useEffect, memo } from "react";
import Grid from "@material-ui/core/Grid";
import { useStore } from "effector-react";
import { SnackbarProvider } from "notistack";

// layouts
import CustomSnackbar from "./layouts/CustomSnackbar";

// pages
import Auth from "./pages/Auth";

import SnackbarContext from "../contexts/SnackbarContext";

// store
import { auth } from "../stores/auth";

import AdminPanel from "./AdminPanel";

const App = () => {
  const { isAuthenticated } = useStore(auth);

  return (
    <div className="app">
      <SnackbarProvider maxSnack={3}>
        <>{isAuthenticated ? <AdminPanel /> : <Auth />}</>
      </SnackbarProvider>
    </div>
  );
};

export default App;
