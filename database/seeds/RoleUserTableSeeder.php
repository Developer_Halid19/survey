<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

use App\Models\User;
use App\Models\Role;
use App\Models\RoleUser;

class RoleUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $user = User::create([
        'name' => 'HALID',
        'email' => 'halid@mail.ru',
        'password' => Hash::make('123456'),
        'api_token' => null
      ]);

      
      $roleUser = Role::create([
        'name' => 'Пользователь',
        'slug' => 'user'
      ]);

      $roleModerator = Role::create([
        'name' => 'Модератор',
        'slug' => 'moderator'
      ]);

      $roleAdmin = Role::create([
        'name' => 'Администратор',
        'slug' => 'administrator'
      ]);



      $user->roles()->attach($roleUser->id);
      $user->roles()->attach($roleModerator->id);
      $user->roles()->attach($roleAdmin->id);
    } 
}
